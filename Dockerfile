FROM alpine:latest

WORKDIR /app

COPY app ./

RUN chmod +x /app/wireproxy

EXPOSE 1080/tcp

CMD ["./wireproxy", "-c", "/app/wireproxy.conf"]
